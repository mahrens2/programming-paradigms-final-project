console.log('page load - entered main.js for js-other api');

var submitButton = document.getElementById('bsr-submit-button');
submitButton.onmouseup = getFormInfo;

function getFormInfo(){
    console.log('entered get form info');
    var action = "YES"; // default
    if (document.getElementById('radio-yes').checked){
        action = "YES";
}   else if (document.getElementById('radio-no').checked) {
        action = "NO";
}
    makeNetworkCallToDrinkApi(action);
} // end of getFormInfo

function makeNetworkCallToDrinkApi(action){
    console.log('entered makeNetworkCallToDrinkApi');
    var url = "http://student12.cse.nd.edu:51042/random";
    var xhr = new XMLHttpRequest(); // 1. creating req
    xhr.open("GET", url, true); // 2. configure request attributes

    // set up onload - triggered when nw response is received
    // must be defined before making the network call
    xhr.onload = function(e){
        console.log('network response received' + xhr.responseText);
        // do something
        updateDrinkWithResponse(action, xhr.responseText);
    } // end of onload

    // set up onerror - triggered if nw response is error response
    xhr.onerror = function(e){
        console.error(xhr.statusText);
    } // end of onerror

    xhr.send(null); // actually send req with no message body
} // end of makeNetworkCallToAgeApi

function updateDrinkWithResponse(action, response_text){
    // extract json info from response
    var response_json = JSON.parse(response_text);
    // update label with it
    var label1 = document.getElementById('response-line1');
    var label2 = document.getElementById('response-line2');
    var label3 = document.getElementById('response-line3');
    var label4 = document.getElementById('response-img1');

    if(response_json['ID'] == null){
        label1.innerHTML = 'Apologies, we could not find your name.';
    } else{
        label1.innerHTML = 'Drink: ' + response_json['Name'];
        label2.innerHTML = 'Glass: ' + response_json['Glass'];
        label3.innerHTML = 'Instructions: ' + response_json['Instructions'];
        label4.src = response_json['DrinkThumb'];

        // make nw call to number api
        if(action == 'YES'){
        
            var name = response_json['Name'];
            var pname = name.split(' ');
            var fname = pname[0];
            console.log('calll to gapi' + pname);
            makeNetworkCallToGenderApi(fname);
        }
    }

} // end of updateAgeWithResponse

function makeNetworkCallToGenderApi(fname){
    console.log('entered makeNetworkCallToGenderApi');
    var url = "https://api.genderize.io/?name=" + fname;
    var xhr = new XMLHttpRequest();
    xhr.open("GET", url, true);

    xhr.onload = function(e) {
        console.log('got nw response from gender api');
        updateTriviaWithResponse(fname,xhr.responseText);
    }

    xhr.onerror = function(e){
        console.error(xhr.statusText);
    }

    xhr.send(null); // send request without bosy
} // end of makeNetworkCallToNumbersApi

function updateTriviaWithResponse(fname, response_text){
    var response_json = JSON.parse(response_text);
    var label4 = document.getElementById('response-line4');
    
    if( response_json['gender'] == null){
        label4.innerHTML = 'Could not decide gender of drink'
    }
    else{
        label4.innerHTML = 'Gender: ' + response_json['gender'] + ', Probability: ' + response_json['probability'];
    }
    // dynamically adding a label
   // label_item = document.createElement("label"); // weird - "label" is a class name
   // label_item.setAttribute("id", "dynamic-label" ); // setAttribute(property_name, value) so here id is property name of button object

   // var item_text = document.createTextNode(response_text);
   // label_item.appendChild(item_text);

    // option 1: directly add to document
    // adding label to document
    //document.body.appendChild(label_item);

    // option 2:
    // adding label as sibling to paragraphs
    //var response_div = document.getElementById("response-div");
    //response_div.appendChild(label_item);

} // end of updateTriviaWithResponse
