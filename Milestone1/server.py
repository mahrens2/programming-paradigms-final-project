import cherrypy
from drinkController import DrinkController
from resetController import ResetController
from drinks_lib import _drink_database

def start_service():
 dispatcher = cherrypy.dispatch.RoutesDispatcher()

 drinkdb = _drink_database()

 drinkController = DrinkController(drinkdb=drinkdb)
 resetController = ResetController(drinkdb=drinkdb)



 dispatcher.connect('drink_get', '/drinks/:drinkID', controller=drinkController, action = 'GET_KEY', conditions=dict(method=['GET']))
 dispatcher.connect('drink_put', '/drinks/:drinkID', controller=drinkController, action = 'PUT_KEY', conditions=dict(method=['PUT']))
 dispatcher.connect('drink_delete', '/drinks/:drinkID', controller=drinkController, action = 'DELETE_KEY', conditions=dict(method=['DELETE']))
 dispatcher.connect('drink_index_get', '/drinks/', controller=drinkController, action = 'GET_INDEX', conditions=dict(method=['GET']))
 dispatcher.connect('drink_index_post', '/drinks/', controller=drinkController, action = 'POST_INDEX', conditions=dict(method=['POST']))
 dispatcher.connect('drink_index_delete', '/drinks/', controller=drinkController, action = 'DELETE_INDEX', conditions=dict(method=['DELETE']))
 dispatcher.connect('drink_rand', '/random', controller=drinkController, action = 'GET_RAND', conditions=dict(method=['GET']))

 dispatcher.connect('reset_put', '/reset/:drinkID', controller=resetController, action = 'PUT_KEY', conditions=dict(method=['PUT']))
 dispatcher.connect('reset_index_put', '/reset/', controller=resetController, action = 'PUT_INDEX', conditions=dict(method=['PUT']))
 dispatcher.connect('drink_options', '/drinks/', controller=optionsController, action='OPTIONS', conditions=dict(method=['OPTIONS']))
 dispatcher.connect('drink_key_options', '/drinks/:drinkID', controller=optionsController, action='OPTIONS', conditions=dict(method=['OPTIONS']))

 conf = {
       'global' : {
                'server.thread_pool': 5,
                'server.socket_host' : 'student04.cse.nd.edu',
                'server.socket_port': 51042
       },
 '/': {
       'request.dispatch' : dispatcher,
       'tools.CORS.on' : True,
        }
 }

 cherrypy.config.update(conf)
 app = cherrypy.tree.mount(None, config=conf)
 cherrypy.quickstart(app)

class optionsController:
    def OPTIONS(self, *args, **kwargs):
        return ""

def CORS():
    cherrypy.response.headers["Access-Control-Allow-Origin"] = "*"
    cherrypy.response.headers["Access-Control-Allow-Methods"] = "GET, PUT, POST, DELETE, OPTIONS"
    cherrypy.response.headers["Access-Control-Allow-Credentials"] = "true"


if __name__ == '__main__':
    cherrypy.tools.CORS = cherrypy.Tool('before_finalize', CORS)
    start_service()
