import cherrypy
import re
import json
import random
from drinks_lib import _drink_database


class DrinkController(object):

    def __init__(self, drinkdb=None):
        if drinkdb is None:
            self.drinkdb = _drink_database()
        else:
            self.drinkdb = drinkdb

        self.drinkdb.load_drinks('DrinkData.json')

    def GET_RAND(self):
        output = {'result' : 'success'}
        drinkIDs = []
        try:
            for drinkID in self.drinkdb.get_drinks():
                drinkIDs.append(drinkID)
            rand = random.choice(drinkIDs)
            drink = self.drinkdb.get_drink(rand)
            if drink is not None:
                                output['ID'] = rand
                                output['Name'] = drink['Name']
                                output['Tags'] = drink['Tags']
                                output['Category'] = drink['Category']
                                output['Alcoholic'] = drink['Alcoholic']
                                output['Glass'] = drink['Glass']
                                output['Instructions'] = drink['Instructions']
                                output['DrinkThumb'] = drink['DrinkThumb']
                                output['Ingredient1'] = drink['Ingredient1']
                                output['Ingredient2'] = drink['Ingredient2']
                                output['Ingredient3'] = drink['Ingredient3']
                                output['Ingredient4'] = drink['Ingredient4']
                                output['Ingredient5'] = drink['Ingredient5']
                                output['Ingredient6'] = drink['Ingredient6']
                                output['Ingredient7'] = drink['Ingredient7']
                                output['Ingredient8'] = drink['Ingredient8']
                                output['Ingredient9'] = drink['Ingredient9']
                                output['Ingredient10'] = drink['Ingredient10']
                                output['Ingredient11'] = drink['Ingredient11']
                                output['Ingredient12'] = drink['Ingredient12']
                                output['Ingredient13'] = drink['Ingredient13']
                                output['Ingredient14'] = drink['Ingredient14']
                                output['Ingredient15'] = drink['Ingredient15']
                                output['Measure1'] = drink['Measure1']
                                output['Measure2'] = drink['Measure2']
                                output['Measure3'] = drink['Measure3']
                                output['Measure4'] = drink['Measure4']
                                output['Measure5'] = drink['Measure5']
                                output['Measure6'] = drink['Measure6']
                                output['Measure7'] = drink['Measure7']
                                output['Measure8'] = drink['Measure8']
                                output['Measure9'] = drink['Measure9']
                                output['Measure10'] = drink['Measure10']
                                output['Measure11'] = drink['Measure11']
                                output['Measure12'] = drink['Measure12']
                                output['Measure13'] = drink['Measure13']
                                output['Measure14'] = drink['Measure14']
                                output['Measure15'] = drink['Measure15']
            else:
                output['result'] = 'error'
                output['message'] = 'drink not found'

        except Exception as ex:
            output['result'] = 'error'
            output['message'] = str(ex)
        return json.dumps(output)


    def GET_KEY(self, drinkID):
        output = {'result': 'success'}
        drinkID = int(drinkID)

        try:
            drink = self.drinkdb.get_drink(drinkID)
            if drink is not None:
                                output['ID'] = drinkID
                                output['Name'] = drink['Name']
                                output['Tags'] = drink['Tags']
                                output['Category'] = drink['Category']
                                output['Alcoholic'] = drink['Alcoholic']
                                output['Glass'] = drink['Glass']
                                output['Instructions'] = drink['Instructions']
                                output['DrinkThumb'] = drink['DrinkThumb']
                                output['Ingredient1'] = drink['Ingredient1']
                                output['Ingredient2'] = drink['Ingredient2']
                                output['Ingredient3'] = drink['Ingredient3']
                                output['Ingredient4'] = drink['Ingredient4']
                                output['Ingredient5'] = drink['Ingredient5']
                                output['Ingredient6'] = drink['Ingredient6']
                                output['Ingredient7'] = drink['Ingredient7']
                                output['Ingredient8'] = drink['Ingredient8']
                                output['Ingredient9'] = drink['Ingredient9']
                                output['Ingredient10'] = drink['Ingredient10']
                                output['Ingredient11'] = drink['Ingredient11']
                                output['Ingredient12'] = drink['Ingredient12']
                                output['Ingredient13'] = drink['Ingredient13']
                                output['Ingredient14'] = drink['Ingredient14']
                                output['Ingredient15'] = drink['Ingredient15']
                                output['Measure1'] = drink['Measure1']
                                output['Measure2'] = drink['Measure2']
                                output['Measure3'] = drink['Measure3']
                                output['Measure4'] = drink['Measure4']
                                output['Measure5'] = drink['Measure5']
                                output['Measure6'] = drink['Measure6']
                                output['Measure7'] = drink['Measure7']
                                output['Measure8'] = drink['Measure8']
                                output['Measure9'] = drink['Measure9']
                                output['Measure10'] = drink['Measure10']
                                output['Measure11'] = drink['Measure11']
                                output['Measure12'] = drink['Measure12']
                                output['Measure13'] = drink['Measure13']
                                output['Measure14'] = drink['Measure14']
                                output['Measure15'] = drink['Measure15']
            else:
                output['result'] = 'error'
                output['message'] = 'drink not found'

        except Exception as ex:
            output['result'] = 'error'
            output['message'] = str(ex)
        return json.dumps(output)

    def PUT_KEY(self, drinkID):
            output = {'result': 'success'}
            drinkID = int(drinkID)

            data = json.loads(cherrypy.request.body.read().decode('utf-8'))

            drink = dict()
            drink['Name'] = data['Name']
            drink['Tags'] = data['Tags']
            drink['Category'] = data['Category']
            drink['Alcoholic'] = data['Alcoholic']
            drink['Glass'] = data['Glass']
            drink['Instructions'] = data['Instructions']
            drink['DrinkThumb'] = data['DrinkThumb']
            drink['Ingredient1'] = data['Ingredient1']
            drink['Ingredient2'] = data['Ingredient2']
            drink['Ingredient3'] = data['Ingredient3']
            drink['Ingredient4'] = data['Ingredient4']
            drink['Ingredient5'] = data['Ingredient5']
            drink['Ingredient6'] = data['Ingredient6']
            drink['Ingredient7'] = data['Ingredient7']
            drink['Ingredient8'] = data['Ingredient8']
            drink['Ingredient9'] = data['Ingredient9']
            drink['Ingredient10'] = data['Ingredient10']
            drink['Ingredient11'] = data['Ingredient11']
            drink['Ingredient12'] = data['Ingredient12']
            drink['Ingredient13'] = data['Ingredient13']
            drink['Ingredient14'] = data['Ingredient14']
            drink['Ingredient15'] = data['Ingredient15']
            drink['Measure1'] = data['Measure1']
            drink['Measure2'] = data['Measure2']
            drink['Measure3'] = data['Measure3']
            drink['Measure4'] = data['Measure4']
            drink['Measure5'] = data['Measure5']
            drink['Measure6'] = data['Measure6']
            drink['Measure7'] = data['Measure7']
            drink['Measure8'] = data['Measure8']
            drink['Measure9'] = data['Measure9']
            drink['Measure10'] = data['Measure10']
            drink['Measure11'] = data['Measure11']
            drink['Measure12'] = data['Measure12']
            drink['Measure13'] = data['Measure13']
            drink['Measure14'] = data['Measure14']
            drink['Measure15'] = data['Measure15']
            self.drinkdb.set_drink(drinkID, drink)
            return json.dumps(output)

    def DELETE_KEY(self, drinkID):

        output = {'result' : 'success'}
        drinkID = int(drinkID)

        try:
            self.drinkdb.delete_drink(drinkID)
        except Exception as ex:
            output['result'] = 'failure'
            output['message'] = str(ex)
        return json.dumps(output)

    def GET_INDEX(self):
        output = {'result' : 'success'}
        output['drinks'] = []
        try:
            for drinkID in self.drinkdb.get_drinks():
                drink = self.drinkdb.get_drink(drinkID)
                output['drinks'].append(drink)
        except Exception as ex:
            output['result'] = 'error'
            output['message'] = str(ex)
        return json.dumps(output)

    def POST_INDEX(self):
        output = {'result':'success'}
        data = json.loads(cherrypy.request.body.read().decode('utf-8'))
        try:
            drinks = sorted(list(self.drinkdb.get_drinks()))
            drinkID = int(drinks[-1]) + 1
            self.drinkdb.DrinkName[drinkID] = data['Name']
            self.drinkdb.Tags[drinkID] = data['Tags']
            self.drinkdb.Category[drinkID] = data['Category']
            self.drinkdb.Alcoholic[drinkID] = data['Alcoholic']
            self.drinkdb.Glass[drinkID] = data['Glass']
            self.drinkdb.Instructions[drinkID] = data['Instructions']
            self.drinkdb.DrinkThumb[drinkID] = data['DrinkThumb']
            self.drinkdb.Ingredient1[drinkID] = data['Ingredient1']
            self.drinkdb.Ingredient2[drinkID] = data['Ingredient2']
            self.drinkdb.Ingredient3[drinkID] = data['Ingredient3']
            self.drinkdb.Ingredient4[drinkID] = data['Ingredient4']
            self.drinkdb.Ingredient5[drinkID] = data['Ingredient5']
            self.drinkdb.Ingredient6[drinkID] = data['Ingredient6']
            self.drinkdb.Ingredient7[drinkID] = data['Ingredient7']
            self.drinkdb.Ingredient8[drinkID] = data['Ingredient8']
            self.drinkdb.Ingredient9[drinkID] = data['Ingredient9']
            self.drinkdb.Ingredient10[drinkID] = data['Ingredient10']
            self.drinkdb.Ingredient11[drinkID] = data['Ingredient11']
            self.drinkdb.Ingredient12[drinkID] = data['Ingredient12']
            self.drinkdb.Ingredient13[drinkID] = data['Ingredient13']
            self.drinkdb.Ingredient14[drinkID] = data['Ingredient14']
            self.drinkdb.Ingredient15[drinkID] = data['Ingredient15']
            self.drinkdb.Measure1[drinkID] = data['Measure1']
            self.drinkdb.Measure2[drinkID] = data['Measure2']
            self.drinkdb.Measure3[drinkID] = data['Measure3']
            self.drinkdb.Measure4[drinkID] = data['Measure4']
            self.drinkdb.Measure5[drinkID] = data['Measure5']
            self.drinkdb.Measure6[drinkID] = data['Measure6']
            self.drinkdb.Measure7[drinkID] = data['Measure7']
            self.drinkdb.Measure8[drinkID] = data['Measure8']
            self.drinkdb.Measure9[drinkID] = data['Measure9']
            self.drinkdb.Measure10[drinkID] = data['Measure10']
            self.drinkdb.Measure11[drinkID] = data['Measure11']
            self.drinkdb.Measure12[drinkID] = data['Measure12']
            self.drinkdb.Measure13[drinkID] = data['Measure13']
            self.drinkdb.Measure14[drinkID] = data['Measure14']
            self.drinkdb.Measure15[drinkID] = data['Measure15']
            output['ID'] = drinkID

        except Exception as ex:
            output['result'] = 'failure'
            output['message'] = str(ex)
        return json.dumps(output)

    def DELETE_INDEX(self):
        output = {'result' : 'success'}
        try:
            for drinkID in list(self.drinkdb.get_drinks()):
                self.drinkdb.delete_drink(drinkID)
        except Exception as ex:
            output['result'] ='failure'
            output['message'] = str(ex)
        return json.dumps(output)
