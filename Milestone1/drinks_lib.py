import re, json

class _drink_database:

    def __init__(self):
        self.DrinkName = dict()
        self.Tags  = dict()
        self.Category = dict()
        self.Alcoholic  = dict()
        self.Glass = dict()
        self.Instructions = dict()
        self.DrinkThumb = dict()
        self.Ingredient1 = dict()
        self.Ingredient2 = dict()
        self.Ingredient3 = dict()
        self.Ingredient4 = dict()
        self.Ingredient5 = dict()
        self.Ingredient6 = dict()
        self.Ingredient7 = dict()
        self.Ingredient8 = dict()
        self.Ingredient9 = dict()
        self.Ingredient10 = dict()
        self.Ingredient11 = dict()
        self.Ingredient12 = dict()
        self.Ingredient13 = dict()
        self.Ingredient14 = dict()
        self.Ingredient15 = dict()
        self.Measure1 = dict()
        self.Measure2 = dict()
        self.Measure3 = dict()
        self.Measure4 = dict()
        self.Measure5 = dict()
        self.Measure6 = dict()
        self.Measure7 = dict()
        self.Measure8 = dict()
        self.Measure9 = dict()
        self.Measure10 = dict()
        self.Measure11 = dict()
        self.Measure12 = dict()
        self.Measure13 = dict()
        self.Measure14 = dict()
        self.Measure15 = dict()

    def load_drinks(self, drink_file):
        f = open(drink_file, "r")
        data = json.load(f)
        for drink in data['drinks']:
           drinkID = int(drink['idDrink'])
           DrinkName = drink['strDrink']
           Tags = drink['strTags']
           Category = drink['strCategory']
           Alcoholic = drink['strAlcoholic']
           Glass = drink['strGlass']
           Instructions = drink['strInstructions']
           DrinkThumb = drink['strDrinkThumb']
           Ingredient1 = drink['strIngredient1']
           Ingredient2 = drink['strIngredient2']
           Ingredient3 = drink['strIngredient3']
           Ingredient4 = drink['strIngredient4']
           Ingredient5 = drink['strIngredient5']
           Ingredient6 = drink['strIngredient6']
           Ingredient7 = drink['strIngredient7']
           Ingredient8 = drink['strIngredient8']
           Ingredient9 = drink['strIngredient9']
           Ingredient10 = drink['strIngredient10']
           Ingredient11 = drink['strIngredient11']
           Ingredient12 = drink['strIngredient12']
           Ingredient13 = drink['strIngredient13']
           Ingredient14 = drink['strIngredient14']
           Ingredient15 = drink['strIngredient15']
           Measure1 = drink['strMeasure1']
           Measure2 = drink['strMeasure2']
           Measure3 = drink['strMeasure3']
           Measure4 = drink['strMeasure4']
           Measure5 = drink['strMeasure5']
           Measure6 = drink['strMeasure6']
           Measure7 = drink['strMeasure7']
           Measure8 = drink['strMeasure8']
           Measure9 = drink['strMeasure9']
           Measure10 = drink['strMeasure10']
           Measure11 = drink['strMeasure11']
           Measure12 = drink['strMeasure12']
           Measure13 = drink['strMeasure13']
           Measure14 = drink['strMeasure14']
           Measure15  = drink['strMeasure15']

           self.DrinkName[drinkID] = DrinkName
           self.Tags[drinkID] = Tags
           self.Category[drinkID] = Category
           self.Alcoholic[drinkID] = Alcoholic
           self.Glass[drinkID] = Glass
           self.Instructions[drinkID] = Instructions
           self.DrinkThumb[drinkID] = DrinkThumb
           self.Ingredient1[drinkID] = Ingredient1
           self.Ingredient2[drinkID] = Ingredient2
           self.Ingredient3[drinkID] = Ingredient3
           self.Ingredient4[drinkID] = Ingredient4
           self.Ingredient5[drinkID] = Ingredient5
           self.Ingredient6[drinkID] = Ingredient6
           self.Ingredient7[drinkID] = Ingredient7
           self.Ingredient8[drinkID] = Ingredient8
           self.Ingredient9[drinkID] = Ingredient9
           self.Ingredient10[drinkID] = Ingredient10
           self.Ingredient11[drinkID] = Ingredient11
           self.Ingredient12[drinkID] = Ingredient12
           self.Ingredient13[drinkID] = Ingredient13
           self.Ingredient14[drinkID] = Ingredient14
           self.Ingredient15[drinkID] = Ingredient15
           self.Measure1[drinkID] = Measure1
           self.Measure2[drinkID] = Measure2
           self.Measure3[drinkID] = Measure3
           self.Measure4[drinkID] = Measure4
           self.Measure5[drinkID] = Measure5
           self.Measure6[drinkID] = Measure6
           self.Measure7[drinkID] = Measure7
           self.Measure8[drinkID] = Measure8
           self.Measure9[drinkID] = Measure9
           self.Measure10[drinkID] = Measure10
           self.Measure11[drinkID] = Measure11
           self.Measure12[drinkID] = Measure12
           self.Measure13[drinkID] = Measure13
           self.Measure14[drinkID] = Measure14
           self.Measure15[drinkID] = Measure15
        f.close()

    def get_drinks(self):
        return self.DrinkName.keys()

    def get_drink(self,drinkID):
        try:
            drink = dict()
            drink['ID'] = int(drinkID)
            drink['Name'] = self.DrinkName[drinkID]
            drink['Tags'] = self.Tags[drinkID]
            drink['Category'] = self.Category[drinkID]
            drink['Alcoholic'] = self.Alcoholic[drinkID]
            drink['Glass'] = self.Glass[drinkID]
            drink['Instructions'] = self.Instructions[drinkID]
            drink['DrinkThumb'] = self.DrinkThumb[drinkID]
            drink['Ingredient1'] = self.Ingredient1[drinkID]
            drink['Ingredient2'] = self.Ingredient2[drinkID]
            drink['Ingredient3'] = self.Ingredient3[drinkID]
            drink['Ingredient4'] = self.Ingredient4[drinkID]
            drink['Ingredient5'] = self.Ingredient5[drinkID]
            drink['Ingredient6'] = self.Ingredient6[drinkID]
            drink['Ingredient7'] = self.Ingredient7[drinkID]
            drink['Ingredient8'] = self.Ingredient8[drinkID]
            drink['Ingredient9'] = self.Ingredient9[drinkID]
            drink['Ingredient10'] = self.Ingredient10[drinkID]
            drink['Ingredient11'] = self.Ingredient11[drinkID]
            drink['Ingredient12'] = self.Ingredient12[drinkID]
            drink['Ingredient13'] = self.Ingredient13[drinkID]
            drink['Ingredient14'] = self.Ingredient14[drinkID]
            drink['Ingredient15'] = self.Ingredient15[drinkID]
            drink['Measure1'] = self.Measure1[drinkID]
            drink['Measure2'] = self.Measure2[drinkID]
            drink['Measure3'] = self.Measure3[drinkID]
            drink['Measure4'] = self.Measure4[drinkID]
            drink['Measure5'] = self.Measure5[drinkID]
            drink['Measure6'] = self.Measure6[drinkID]
            drink['Measure7'] = self.Measure7[drinkID]
            drink['Measure8'] = self.Measure8[drinkID]
            drink['Measure9'] = self.Measure9[drinkID]
            drink['Measure10'] = self.Measure10[drinkID]
            drink['Measure11'] = self.Measure11[drinkID]
            drink['Measure12'] = self.Measure12[drinkID]
            drink['Measure13'] = self.Measure13[drinkID]
            drink['Measure14'] = self.Measure14[drinkID]
            drink['Measure15'] = self.Measure15[drinkID]

        except Exception as ex:
            drink = None
        return drink

    def set_drink(self,drinkID,drink):
        self.DrinkName[drinkID] = drink['Name']
        self.Tags[drinkID] = drink['Tags']
        self.Category[drinkID] = drink['Category']
        self.Alcoholic[drinkID] = drink['Alcoholic']
        self.Glass[drinkID] = drink['Glass']
        self.Instructions[drinkID] = drink['Instructions']
        self.DrinkThumb[drinkID] = drink['DrinkThumb']
        self.Ingredient1[drinkID] = drink['Ingredient1']
        self.Ingredient2[drinkID] = drink['Ingredient2']
        self.Ingredient3[drinkID] = drink['Ingredient3']
        self.Ingredient4[drinkID] = drink['Ingredient4']
        self.Ingredient5[drinkID] = drink['Ingredient5']
        self.Ingredient6[drinkID] = drink['Ingredient6']
        self.Ingredient7[drinkID] = drink['Ingredient7']
        self.Ingredient8[drinkID] = drink['Ingredient8']
        self.Ingredient9[drinkID] = drink['Ingredient9']
        self.Ingredient10[drinkID] = drink['Ingredient10']
        self.Ingredient11[drinkID] = drink['Ingredient11']
        self.Ingredient12[drinkID] = drink['Ingredient12']
        self.Ingredient13[drinkID] = drink['Ingredient13']
        self.Ingredient14[drinkID] = drink['Ingredient14']
        self.Ingredient15[drinkID] = drink['Ingredient15']
        self.Measure1[drinkID] = drink['Measure1']
        self.Measure2[drinkID] = drink['Measure2']
        self.Measure3[drinkID] = drink['Measure3']
        self.Measure4[drinkID] = drink['Measure4']
        self.Measure5[drinkID] = drink['Measure5']
        self.Measure6[drinkID] = drink['Measure6']
        self.Measure7[drinkID] = drink['Measure7']
        self.Measure8[drinkID] = drink['Measure8']
        self.Measure9[drinkID] = drink['Measure9']
        self.Measure10[drinkID] = drink['Measure10']
        self.Measure11[drinkID] = drink['Measure11']
        self.Measure12[drinkID] = drink['Measure12']
        self.Measure13[drinkID] = drink['Measure13']
        self.Measure14[drinkID] = drink['Measure14']
        self.Measure15[drinkID] = drink['Measure15']

    def delete_drink(self,drinkID):
        del self.DrinkName[drinkID]
        del self.Tags[drinkID]
        del self.Category[drinkID]
        del self.Alcoholic[drinkID]
        del self.Glass[drinkID]
        del self.Instructions[drinkID]
        del self.DrinkThumb[drinkID]
        del self.Ingredient1[drinkID]
        del self.Ingredient2[drinkID]
        del self.Ingredient3[drinkID]
        del self.Ingredient4[drinkID]
        del self.Ingredient5[drinkID]
        del self.Ingredient6[drinkID]
        del self.Ingredient7[drinkID]
        del self.Ingredient8[drinkID]
        del self.Ingredient9[drinkID]
        del self.Ingredient10[drinkID]
        del self.Ingredient11[drinkID]
        del self.Ingredient12[drinkID]
        del self.Ingredient13[drinkID]
        del self.Ingredient14[drinkID]
        del self.Ingredient15[drinkID]
        del self.Measure1[drinkID]
        del self.Measure2[drinkID]
        del self.Measure3[drinkID]
        del self.Measure4[drinkID]
        del self.Measure5[drinkID]
        del self.Measure6[drinkID]
        del self.Measure7[drinkID]
        del self.Measure8[drinkID]
        del self.Measure9[drinkID]
        del self.Measure10[drinkID]
        del self.Measure11[drinkID]
        del self.Measure12[drinkID]
        del self.Measure13[drinkID]
        del self.Measure14[drinkID]
        del self.Measure15[drinkID]

    def delete_all_drinks(self):
        for drinkID in self.get_drinks():
            self.delete_drink(drinkID)

if __name__ == "__main__":

    drinkdb = _drink_database()

    drinkdb.load_drinks('DrinkData.json')
    print(drinkdb.get_drink(12452))
