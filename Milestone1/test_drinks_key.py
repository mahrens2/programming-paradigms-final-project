import unittest
import requests
import json

class TestDrinks(unittest.TestCase):

    SITE_URL = 'localhost:51042' 
    print("testing for server: " + SITE_URL)
    DRINKS_URL = SITE_URL + '/drinks/'
    RESET_URL = SITE_URL + '/reset/'

    def reset_data(self):
        m = {}
        r = requests.put(self.RESET_URL, data = json.dumps(m))


    def is_json(self, resp):
        try:
            json.loads(resp)
            return True
        except ValueError:
            return False

    def test_drinks_get_key(self):
        self.reset_data()
        drink_id = 13940
        r = requests.get(self.DRINKS_URL + str(drink_id))
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode('utf-8'))
        self.assertEqual(resp['Name'], '69 Special')
        self.assertEqual(resp['Ingredient2'], '7-Up')

    def test_drinks_put_key(self):
        self.reset_data()
        drink_id = 13940

        r = requests.get(self.DRINKS_URL + str(drink_id))
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode('utf-8'))
        self.assertEqual(resp['Name'], '69 Special')
        self.assertEqual(resp['Ingredient2'], '7-Up')

        m = dict()
        m['Name'] = 'ABC'
        m['Ingredient1'] = 'apples and bannanas'
        m['Tags'] = None
        m['Category'] = None
        m['Alcoholic'] = None
        m['Glass'] = None
        m['Instructions'] = None
        m['DrinkThumb'] = None
        m['Ingredient2'] = None
        m['Ingredient3'] = None
        m['Ingredient4'] = None
        m['Ingredient5'] = None
        m['Ingredient6'] = None
        m['Ingredient7'] = None
        m['Ingredient8'] = None
        m['Ingredient9'] = None
        m['Ingredient10'] = None
        m['Ingredient11'] = None
        m['Ingredient12'] = None
        m['Ingredient13'] = None
        m['Ingredient14'] = None
        m['Ingredient15'] = None
        m['Measure1'] = None
        m['Measure2'] = None
        m['Measure3'] = None
        m['Measure4'] = None
        m['Measure5'] = None
        m['Measure6'] = None
        m['Measure7'] = None
        m['Measure8'] = None
        m['Measure9'] = None
        m['Measure10'] = None
        m['Measure11'] = None
        m['Measure12'] = None
        m['Measure13'] = None
        m['Measure14'] = None
        m['Measure15'] = None
        r = requests.put(self.DRINKS_URL + str(drink_id), data = json.dumps(m))
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode('utf-8'))
        self.assertEqual(resp['result'], 'success')

        r = requests.get(self.DRINKS_URL + str(drink_id))
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode('utf-8'))
        self.assertEqual(resp['Name'], m['Name'])
        self.assertEqual(resp['Ingredient1'], m['Ingredient1'])

    def test_drinks_delete_key(self):
        self.reset_data()
        drink_id = 13940

        m = {}
        r = requests.delete(self.DRINKS_URL + str(drink_id), data = json.dumps(m))
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode('utf-8'))
        self.assertEqual(resp['result'], 'success')

        r = requests.get(self.DRINKS_URL + str(drink_id))
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode('utf-8'))
        self.assertEqual(resp['result'], 'error')

if __name__ == "__main__":
    unittest.main()
