import cherrypy
import re, json
from drinks_lib import _drink_database

class ResetController(object):

	def __init__(self, drinkdb=None):
		if drinkdb is None:
			self.drinkdb = _drink_database()
		else:
			self.drinkdb = drinkdb

	def PUT_INDEX(self):
		output = {'result' : 'success'}

		data = json.loads(cherrypy.request.body.read().decode())

		self.drinkdb.__init__()
		self.drinkdb.load_drinks('DrinkData.json')

		return json.dumps(output)

	def PUT_KEY(self, drinkID):
		output = {'result' : 'success'}
		drinkID = int(drinkID)

		try:
			data = json.loads(cherrypy.request.body.read().decode())

			drinkdbtmp = _drink_database()
			drinkdbtmp.load_drinks('DrinkData.json')

			drink = drinkdbtmp.get_drink(drinkID)

			self.drinkdb.set_drink(drinkID, drink)

		except Exception as ex:
			output['result'] = 'error'
			output['message'] = str(ex)

		return json.dumps(output)
