import unittest
import requests
import json


class TestReset(unittest.TestCase):

    SITE_URL = 'localhost:51042'
    print('Testing for server: ' + SITE_URL)
    RESET_URL = SITE_URL + '/reset/'

    def test_put_reset_index(self):
        m = {}
        r = requests.put(self.RESET_URL, json.dumps(m))
        resp = json.loads(r.content.decode())
        self.assertEqual(resp['result'], 'success')
        r = requests.get(self.SITE_URL + '/drinks/')
        resp = json.loads(r.content.decode())
        drinks = resp['drinks']
        for drink in drinks:
            if drink['ID'] == 12452:
                testdrink = drink

        self.assertEqual(testdrink['Name'], 'Victory Collins')
        self.assertEqual(testdrink['Ingredient1'], 'Vodka')

    def test_put_reset_key(self):
        m = dict()
        r = requests.put(self.RESET_URL, json.dumps(m))

        drink_id = 12452
        m['Name'] = 'ABC'
        m['Ingredient1'] = 'apples and bannanas'
        m['Tags'] = None
        m['Category'] = None
        m['Alcoholic'] = None
        m['Glass'] = None
        m['Instructions'] = None
        m['DrinkThumb'] = None
        m['Ingredient2'] = None
        m['Ingredient3'] = None
        m['Ingredient4'] = None
        m['Ingredient5'] = None
        m['Ingredient6'] = None
        m['Ingredient7'] = None
        m['Ingredient8'] = None
        m['Ingredient9'] = None
        m['Ingredient10'] = None
        m['Ingredient11'] = None
        m['Ingredient12'] = None
        m['Ingredient13'] = None
        m['Ingredient14'] = None
        m['Ingredient15'] = None
        m['Measure1'] = None
        m['Measure2'] = None
        m['Measure3'] = None
        m['Measure4'] = None
        m['Measure5'] = None
        m['Measure6'] = None
        m['Measure7'] = None
        m['Measure8'] = None
        m['Measure9'] = None
        m['Measure10'] = None
        m['Measure11'] = None
        m['Measure12'] = None
        m['Measure13'] = None
        m['Measure14'] = None
        m['Measure15'] = None
        r = requests.put(self.SITE_URL + '/drinks/' + str(drink_id), data=json.dumps(m))

        m = {}
        r = requests.put(self.RESET_URL + str(drink_id), data=json.dumps(m))
        resp = json.loads(r.content.decode())
        self.assertEqual(resp['result'], 'success')

        r = requests.get(self.SITE_URL + '/drinks/')
        resp = json.loads(r.content.decode())
        drinks = resp['drinks']
        for drink in drinks:
            if drink['ID'] == 12452:
                testdrink = drink

        self.assertEqual(testdrink['Name'], 'Victory Collins')
        self.assertEqual(testdrink['Ingredient1'], 'Vodka')


if __name__ == '__main__':
    unittest.main()
