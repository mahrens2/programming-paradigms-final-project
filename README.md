# Programming-paradigms-final-project

Slides: https://docs.google.com/presentation/d/1lpBHoMz6MSdX0RStD42rrhuLTKEG8AjWT84sfAgJe_A/edit?usp=sharing

**Running the Server**

The server for our website is currently hosted at student04.cse.nd.edu:51042. This is also the host the JavaScript that interacts with the server is set to send and receive requests from. The host can be changed by editing the host value in Milstone1/server.py and the host value in all scripts in jsfrontend/pages/scripts. 

All files required for the server are contained in the Milestone1 directory.

How to run the server:

    - ssh into the student04 machine
    - navigate to the Milestone1 directory
    - enter the command `python3 server.py`

After this the server should be up and running, and you should be able to access the homepage of the website at http://mahrens2.gitlab.io/programming-paradigms-final-project/jsfrontend/pages/home.html

**Unit Tests**

To run the unit tests you must first start the server: python3 server.py, then you can run each test using: python3 "test_file.py"


**JSON specification**

| Request Type | Resource endpoint | Body | Expected response | Inner working |
|-|-|-|-|-|
| GET | /drinks/ | No body | String formatted json of all drinks, See below for single response example | GET_INDEX Goes through and gets all drinks and their info, uses get_drinks() and get_drink() from drinkdb |
| GET | /drinks/:drinkID | No body | String formatted json of named drink Ex: {"result": "success", "ID": 13940, "Name": "69 Special", "Tags": null, "Category": "Ordinary Drink", "Alcoholic": "Alcoholic", "Glass": "Collins Glass", "Instructions": "Pour 2 oz. gin. Add 4 oz. 7-up. Add Lemon Juice for flavor. If you are weak, top up glass with more 7-Up.", "DrinkThumb": "https://www.thecocktaildb.com/images/media/drink/vqyxqx1472669095.jpg", "Ingredient1": "Gin", "Ingredient2": "7-Up", "Ingredient3": "Lemon juice", "Ingredient4": null, ect …}’ | GET_DRINK Gets a specific drink by ID, uses get_drink() from drinkdb  |
| PUT | /drinks/:id | ‘{“strDrink": "Ziemes Martini Apfelsaft","Category": "Ordinary Drink", "Alcoholic": "Alcoholic", "Glass": "Collins Glass"}’ | {“result”:”success”} if operation works | PUT_DRINK Updates a drink entry using the drinkID and put_drink() |
| POST | /drinks/ | ‘{”strDrink”:”Margarita”, ect.}’ | {“result”:”success”, “id”:”newid”} if operation works | POST_DRINK Creates a new drink entry in drinkdb and returns a  new unique id, uses post_drink() |
| DELETE | /drinks/:id | ‘{}’ | {“result”:”success”} if operation works | DELETE_DRINK,  deletes a drink from drinkdb, uses delete_drink() |
| DELETE | /drinks/ | ‘{}’ | {“result”:”success”} if operation works | DELETE_INDEX Deletes all drinks from drinkdb, uses delete_drink() |
| GET | /random | No body | {“result”:”success”} and a random drink returned if operation works | GET_RAND Gets a random drink by ID from drinkdb, uses get_drinks() and get_drink() |
