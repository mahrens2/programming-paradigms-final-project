console.log('page load - entered search.js for drink api');

var submitbtn = document.getElementById('getformbtn');
submitbtn.onclick = getFormInfo;

function getFormInfo(){
    console.log('entered get form info');
    var drtype = "all"; //default
    if(document.getElementById('alc').checked){
        drtype = "Alcoholic";
    }
    else if(document.getElementById('non').checked){
        drtype = "Non alcoholic";
    }

    var namesearch = document.getElementById('searchname').value;
    var ingsearch = document.getElementById('searchingred').value.split(' ');
    console.log('type ' + drtype);
    console.log('name ' + namesearch.length);
    console.log('ing ' + ingsearch);
    console.log('ing0 ' + ingsearch[0].length);
    console.log('ingL ' + ingsearch.length);
    console.log('ing ' + Array.isArray(ingsearch));
    console.log('ing ' + ingsearch + '!');
    
    makeNetCallToDrinkApi(drtype,namesearch,ingsearch);

}

function makeNetCallToDrinkApi(drtype,namesearch,ingsearch){
    console.log('entered makeNetCallToDrinkApi');
    var url = "http://student04.cse.nd.edu:51042/drinks/";
    var xhr = new XMLHttpRequest(); // 1. creating req
    xhr.open("GET", url, true); // 2. configure request attributes

    // set up onload - triggered when nw response is received
    // must be defined before making the network call
    xhr.onload = function(e){
        console.log('network response received' + xhr.responseText);
        // do something
        updateDrinkWithResponse(xhr.responseText,drtype,namesearch,ingsearch);
    } // end of onload

    // set up onerror - triggered if nw response is error response
    xhr.onerror = function(e){
        console.error(xhr.statusText);
    } // end of onerror

    xhr.send(null); // actually send req with no message body
} // end of makeNetworkCallToAgeApi

function updateDrinkWithResponse(response_text,drtype,name,ingredient){
    console.log('entered update');
    // extract json info from response
    var response_json = JSON.parse(response_text);
    // update label with it
    var list = document.getElementById('drinklist');
    
    var fragment = document.createDocumentFragment();
    var drinks = response_json['drinks'];
  
    //handle search type
    if(name.length == 0 && ingredient[0].length == 0){
        searchOnlyDrtype(drinks,fragment,drtype);  
    }
    else if(ingredient[0].length == 0){
        searchDrtypeAndName(drinks,fragment,drtype,name);
    }
    else if(name.length == 0){
        searchDrtypeAndIng(drinks,fragment,drtype,ingredient);
    }
    else{
        searchAll(drinks,fragment,drtype,name,ingredient);
    }

    //reset list and append the returned fragment to display search results
    list.innerHTML = '';
    list.appendChild(fragment);
    
} 

//search for only drink type
function searchOnlyDrtype(drinks,fragment,type){
    console.log('entered searchOnlyDrtype');
    if(type == "all"){
        var indexmsg = document.createElement('h2');
        indexmsg.className = "mt-3 text-center"
        var msgtext = document.createTextNode("Please vist the index page to view all drinks");
        indexmsg.appendChild(msgtext);
        fragment.appendChild(indexmsg);
        return;
    }
    else{ 

        drinks.forEach((drink) => {
              if(drink['Alcoholic'] == type){
                createmedia(drink,fragment);
              }
            
        });
    }

}

function searchDrtypeAndName(drinks,fragment,type,name){
    var found = 0;
    if(type == "all"){
         drinks.forEach((drink) => {
             if(drink['Name'].toLowerCase().includes(name.toLowerCase())){
                createmedia(drink,fragment);
                found++;
             }
         });
    }
    else{
         drinks.forEach((drink) => {
             if(drink['Name'].toLowerCase().includes(name.toLowerCase()) && drink['Alcoholic'] == type){
                createmedia(drink,fragment);
                found++;
              }
         });
    } 

    if(found == 0){
        notFoundmsg(fragment);
    }

}

function searchDrtypeAndIng(drinks,fragment,type,ingredient){
    var found = 0;
    var match = 0;
    if(type == "all"){
         drinks.forEach((drink) => {
            var ingredList = drink['Ingredient1'] +  " " + drink['Ingredient2'] +  " " + drink['Ingredient3'] +  " " +
            drink['Ingredient4'] +  " " + drink['Ingredient5'] +  " " + drink['Ingredient6'] +  " " +
            drink['Ingredient7'] +  " " + drink['Ingredient8'] +  " " + drink['Ingredient9'] +  " " +
            drink['Ingredient10'] +  " " + drink['Ingredient11'] +  " " + drink['Ingredient12'] +  " " +
            drink['Ingredient13'] +  " " + drink['Ingredient14'] +  " " + drink['Ingredient15'];
            
            ingredList = ingredList.replaceAll(/null/g, '');
            
             var i; 
             for(i = 0; i <  ingredient.length; i++){
                 var curIng = ingredient[i].toLowerCase()
                if(ingredList.toLowerCase().includes(curIng)){
                    match++;
                    console.log('matched: ' + drink['Name'] + ingredient[i] + 'Cyr ' + curIng + '#' + match);
                    if(match == ingredient.length){
                        console.log('matched: ' + drink['Name'] + '#m' + match + '#ing' + ingredient.length);
                        createmedia(drink,fragment);
                        found++;
                    }
                }
             }
             match = 0;
         });
    }
    else{
         drinks.forEach((drink) => {
            var ingredList = drink['Ingredient1'] +  " " + drink['Ingredient2'] +  " " + drink['Ingredient3'] +  " " +
            drink['Ingredient4'] +  " " + drink['Ingredient5'] +  " " + drink['Ingredient6'] +  " " +
            drink['Ingredient7'] +  " " + drink['Ingredient8'] +  " " + drink['Ingredient9'] +  " " +
            drink['Ingredient10'] +  " " + drink['Ingredient11'] +  " " + drink['Ingredient12'] +  " " +
            drink['Ingredient13'] +  " " + drink['Ingredient14'] +  " " + drink['Ingredient15'];
            
            ingredList = ingredList.replaceAll(/null/g, '');

              var i; 
             for(i = 0; i <  ingredient.length; i++){
                 var curIng = ingredient[i].toLowerCase()
                if(ingredList.toLowerCase().includes(curIng) && drink['Alcoholic'] == type ){
                    match++;
                    console.log('matched: ' + drink['Name'] + ingredient[i] + 'Cyr ' + curIng + '#' + match);
                    if(match == ingredient.length){
                        console.log('matched: ' + drink['Name'] + '#m' + match + '#ing' + ingredient.length);
                        createmedia(drink,fragment);
                        found++;
                    }
                }
             }
             match = 0;
         });
    } 

    if(found == 0){
        notFoundmsg(fragment);
    }
    
}

function searchAll(drinks,fragment,type,name,ingredient){
     var found = 0;
    var match = 0;
    if(type == "all"){
         drinks.forEach((drink) => {
            var ingredList = drink['Ingredient1'] +  " " + drink['Ingredient2'] +  " " + drink['Ingredient3'] +  " " +
            drink['Ingredient4'] +  " " + drink['Ingredient5'] +  " " + drink['Ingredient6'] +  " " +
            drink['Ingredient7'] +  " " + drink['Ingredient8'] +  " " + drink['Ingredient9'] +  " " +
            drink['Ingredient10'] +  " " + drink['Ingredient11'] +  " " + drink['Ingredient12'] +  " " +
            drink['Ingredient13'] +  " " + drink['Ingredient14'] +  " " + drink['Ingredient15'];
            
            ingredList = ingredList.replaceAll(/null/g, '');
            
             var i; 
             for(i = 0; i <  ingredient.length; i++){
                 var curIng = ingredient[i].toLowerCase()
                if(ingredList.toLowerCase().includes(curIng)){
                    match++;
                    console.log('matched: ' + drink['Name'] + ingredient[i] + 'Cyr ' + curIng + '#' + match);
                    if(match == ingredient.length && drink['Name'].toLowerCase().includes(name.toLowerCase())){
                        console.log('matched: ' + drink['Name'] + '#m' + match + '#ing' + ingredient.length);
                        createmedia(drink,fragment);
                        found++;
                    }
                }
             }
             match = 0;
         });
    }
    else{
         drinks.forEach((drink) => {
            var ingredList = drink['Ingredient1'] +  " " + drink['Ingredient2'] +  " " + drink['Ingredient3'] +  " " +
            drink['Ingredient4'] +  " " + drink['Ingredient5'] +  " " + drink['Ingredient6'] +  " " +
            drink['Ingredient7'] +  " " + drink['Ingredient8'] +  " " + drink['Ingredient9'] +  " " +
            drink['Ingredient10'] +  " " + drink['Ingredient11'] +  " " + drink['Ingredient12'] +  " " +
            drink['Ingredient13'] +  " " + drink['Ingredient14'] +  " " + drink['Ingredient15'];
            
            ingredList = ingredList.replaceAll(/null/g, '');

              var i; 
             for(i = 0; i <  ingredient.length; i++){
                 var curIng = ingredient[i].toLowerCase()
                if(ingredList.toLowerCase().includes(curIng) && drink['Alcoholic'] == type ){
                    match++;
                    console.log('matched: ' + drink['Name'] + ingredient[i] + 'Cyr ' + curIng + '#' + match);
                    if(match == ingredient.length && drink['Name'].toLowerCase().includes(name.toLowerCase())){
                        console.log('matched: ' + drink['Name'] + '#m' + match + '#ing' + ingredient.length);
                        createmedia(drink,fragment);
                        found++;
                    }
                }
             }
             match = 0;
         });
    } 

    if(found == 0){
        notFoundmsg(fragment);
    }
     
    
}
//message for search with no results
function notFoundmsg(fragment){
        fragment.replaceChildren();
        var indexmsg = document.createElement('h2');
        indexmsg.className = "mt-3 text-center"
        var msgtext = document.createTextNode("Sorry, we could not find any drinks matching your criteria");
        indexmsg.appendChild(msgtext);
        fragment.appendChild(indexmsg);
}
//dynamic DOM object creation, makes a media object of drinks that match the search result
function createmedia(drink,fragment){
    
     var media = document.createElement('div');
     media.className = "media my-4 border border-secondary bg-light";
     var mediabody = document.createElement('div');
     mediabody.className = "media-body";
     var drinkhead = document.createElement('h5');
     drinkhead.className = "text-left ml-2 mt-2";
     var mbtext = document.createTextNode(drink['Name']);
     drinkhead.appendChild(mbtext);
     var link = document.createElement('a');
     link.className = "text-dark";
     link.href = "drink.html";
     var mbtags = document.createElement('p');
     mbtags.className = "text-secondary font-italic font-weight-light ml-2";
     var bodytxt = document.createTextNode(drink['Category'] + ", " + drink['Alcoholic'] + ".");
     mbtags.appendChild(bodytxt);

     var mediaimg = document.createElement('img');
     mediaimg.className = "img-thumbnail float-right mt-2 mr-2"
     mediaimg.src = drink['DrinkThumb'];
     mediaimg.style = "width: 64px; height: 64px;"
     media.appendChild(mediabody);
     mediabody.appendChild(drinkhead);
     mediabody.appendChild(mbtags);
     media.appendChild(mediaimg);
     link.appendChild(media);
     

     media.onclick = function () {
         setlink(drink['ID']);
     }
     
    fragment.appendChild(link);

}
//saves a drinks id so it can be retreived by the drink page 
function setlink(id){
    console.log('set link to' + id);
    localStorage.setItem('link',id);
}



