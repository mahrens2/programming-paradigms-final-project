console.log('page load - entered random.js for drink api');

//drink = localStorage.getItem('link');
window.onload = makeNetCallToDrinkApi();
//localStorage.clear();

function makeNetCallToDrinkApi(){
    console.log('entered makeNetCallToDrinkApi');
    var url = "http://student04.cse.nd.edu:51042/drinks/";
    var xhr = new XMLHttpRequest(); // 1. creating req
    xhr.open("GET", url, true); // 2. configure request attributes

    // set up onload - triggered when nw response is received
    // must be defined before making the network call
    xhr.onload = function(e){
        console.log('network response received' + xhr.responseText);
        // do something
        updateDrinkWithResponse(xhr.responseText);
    } // end of onload

    // set up onerror - triggered if nw response is error response
    xhr.onerror = function(e){
        console.error(xhr.statusText);
    } // end of onerror

    xhr.send(null); // actually send req with no message body
} // end of makeNetworkCallToAgeApi

function updateDrinkWithResponse(response_text){
    // extract json info from response
    var response_json = JSON.parse(response_text);
    // update label with it
    var listr = document.getElementById('drinklistr');
    var listl = document.getElementById('drinklistl');
    var fragmentr = document.createDocumentFragment();
    var fragmentl = document.createDocumentFragment();
    var drinks = response_json['drinks'];
    var drinknum = drinks.length;
    var rsidenum = Math.floor(drinknum / 2);
    var i = 0; 
    drinks.forEach((item) => {
        if(i < rsidenum){
            createmedia(item,fragmentr); 
        }
        else{
            createmedia(item,fragmentl);
        }
        i++;    
    });

    listr.appendChild(fragmentr);  
    listl.appendChild(fragmentl);
} 

function createmedia(drink,fragment){
    
     var media = document.createElement('div');
     media.className = "media my-4 border border-secondary bg-light";
     var mediabody = document.createElement('div');
     mediabody.className = "media-body";
     var drinkhead = document.createElement('h5');
     drinkhead.className = "text-left ml-2 mt-2";
     var mbtext = document.createTextNode(drink['Name']);
     drinkhead.appendChild(mbtext);
     var link = document.createElement('a');
     link.className = "text-dark";
     link.href = "drink.html";
     var mbtags = document.createElement('p');
     mbtags.className = "text-secondary font-italic font-weight-light ml-2";
     var bodytxt = document.createTextNode(drink['Category'] + ", " + drink['Alcoholic'] + ".");
     mbtags.appendChild(bodytxt);

     var mediaimg = document.createElement('img');
     mediaimg.className = "img-thumbnail float-right mt-2 mr-2"
     mediaimg.src = drink['DrinkThumb'];
     mediaimg.style = "width: 64px; height: 64px;"
     media.appendChild(mediabody);
     mediabody.appendChild(drinkhead);
     mediabody.appendChild(mbtags);
     media.appendChild(mediaimg);
     link.appendChild(media);
     

     media.onclick = function () {
         setlink(drink['ID']);
     }
     
    fragment.appendChild(link);

}

function setlink(id){
    console.log('set link to' + id);
    localStorage.setItem('link',id);
}



