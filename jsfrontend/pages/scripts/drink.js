console.log('page load - entered random.js for drink api');

drink = localStorage.getItem('link');
window.onload = makeNetCallToDrinkApi(drink);
localStorage.clear();

function makeNetCallToDrinkApi(drink){
    console.log('entered makeNetCallToDrinkApi');
    var url = "http://student04.cse.nd.edu:51042/drinks/" + drink;
    var xhr = new XMLHttpRequest(); // 1. creating req
    xhr.open("GET", url, true); // 2. configure request attributes

    // set up onload - triggered when nw response is received
    // must be defined before making the network call
    xhr.onload = function(e){
        console.log('network response received' + xhr.responseText);
        // do something
        updateDrinkWithResponse(xhr.responseText);
    } // end of onload

    // set up onerror - triggered if nw response is error response
    xhr.onerror = function(e){
        console.error(xhr.statusText);
    } // end of onerror

    xhr.send(null); // actually send req with no message body
} // end of makeNetworkCallToAgeApi

function updateDrinkWithResponse(response_text){
    // extract json info from response
    var response_json = JSON.parse(response_text);
    // update label with it
    var title = document.getElementById('tdname');
    var name = document.getElementById('drinkname');
    var glass = document.getElementById('glasstype');
    var inst = document.getElementById('instructions');
    var ingred = document.getElementById('ingredients');
    var img = document.getElementById('drinkimg');

    if(response_json['ID'] == null){
        name.innerHTML = 'Apologies, we could not find a drink.';
    } else{
        title.innerHTML = response_json['Name'];
        name.innerHTML = response_json['Name'];
        glass.innerHTML = 'Glass: ' + response_json['Glass'];
        inst.innerHTML = 'Instructions: ' + response_json['Instructions'];
        ingred.innerHTML = 'Ingredients: <br><br>' + response_json['Ingredient1'] +  '  ' +  response_json['Measure1'] + '<br>'
        + response_json['Ingredient2'] +  '  ' +  response_json['Measure2'] + '<br>'
        + response_json['Ingredient3'] +  '  ' +  response_json['Measure3'] + '<br>'
        + response_json['Ingredient4'] +  '  ' +  response_json['Measure4'] + '<br>'
        + response_json['Ingredient5'] +  '  ' +  response_json['Measure5'] + '<br>'
        + response_json['Ingredient6'] +  '  ' +  response_json['Measure6'] + '<br>'
        + response_json['Ingredient7'] +  '  ' +  response_json['Measure7'] + '<br>'
        + response_json['Ingredient8'] +  '  ' +  response_json['Measure8'] + '<br>'
        + response_json['Ingredient9'] +  '  ' +  response_json['Measure9'] + '<br>'
        + response_json['Ingredient10'] +  '  ' +  response_json['Measure10'] + '<br>'
        + response_json['Ingredient11'] +  '  ' +  response_json['Measure11'] + '<br>'
        + response_json['Ingredient12'] +  '  ' +  response_json['Measure12'] + '<br>'
        + response_json['Ingredient13'] +  '  ' +  response_json['Measure13'] + '<br>'
        + response_json['Ingredient14'] +  '  ' +  response_json['Measure14'] + '<br>'
        + response_json['Ingredient15'] +  '  ' +  response_json['Measure15'] + '<br>';
        ingred.innerHTML = ingred.innerHTML.replace(/null/g, '');
        img.src = response_json['DrinkThumb'];

    
    }

} 

