console.log('page load - entered home.js for drink api');

window.onload = makeNetCallToDrinkApi;


function makeNetCallToDrinkApi(){
    console.log('entered makeNetCallToDrinkApi');
    var url = "http://student04.cse.nd.edu:51042/random";
    var requests = new Array(5);
    var imgindex = ['cimg1','cimg2','cimg3','cimg4','cimg5'];
    var hindex = ['ch1','ch2','ch3','ch4','ch5'];
    var lindex = ['hr1','hr2','hr3','hr4','hr5'];
    for(let i = 0; i < 5; i++){
        requests[i] = new XMLHttpRequest(); // 1. creating req
        requests[i].open("GET", url, true); // 2. configure request attributes
    // set up onload - triggered when nw response is received
    // must be defined before making the network call
        
        requests[i].onload = function(e){
            console.log('network response received' + requests[i].responseText);
            // do something
            
            updateDrinkWithResponse(requests[i].responseText,imgindex[i],hindex[i],lindex[i]);
        } // end of onload
    
    // set up onerror - triggered if nw response is error response
        requests[i].onerror = function(e){
            console.error(xhr.statusText);
        } // end of onerror

        requests[i].send(null); // actually send req with no message body
    }
} // end of makeNetworkCallToAgeApi

function updateDrinkWithResponse(response_text,img,name,link){
    // extract json info from response
    var response_json = JSON.parse(response_text);
    // update label with it
    var drink = document.getElementById(name);
    var dimg = document.getElementById(img);
    var imlink = document.getElementById(link);
    
    if(response_json['ID'] == null){
        drink.innerHTML = 'Apologies, we could not find a drink.';
    } else{
        drink.innerHTML = response_json['Name'];
        dimg.src = response_json['DrinkThumb'];
        imlink.href = "drink.html"
        dimg.onclick = function(){
            setlink(response_json['ID']);
        }
    }

} 

function setlink(id){
    console.log('set link to' + id);
    localStorage.setItem('link',id);
}


